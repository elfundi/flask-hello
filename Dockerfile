FROM python:3.10-bookworm
WORKDIR /app
COPY app.py /app
COPY index.html /app 
COPY requirements.txt /app
RUN pip install -r requirements.txt
EXPOSE 9090
CMD ["python", "app.py"]
