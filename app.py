from flask import Flask, request, render_template
import platform
import psutil
import socket

app = Flask(__name__, template_folder="./")

@app.route('/', methods=['GET'])
def hello():
  data = {}
  data['hostname'] = socket.gethostname()
  data['machine'] = platform.machine()
  data['system'] = platform.system()
  return render_template("index.html", data=data)


if __name__ == '__main__':
  app.run(host='0.0.0.0', port=9090, debug=True)
